let app;
document.addEventListener("DOMContentLoaded", function () {
  let isFramed = false;
  try {
    isFramed =
      window != window.top ||
      document != top.document ||
      self.location != top.location;
  } catch (e) {
    isFramed = true;
  }
  app = new Vue({
    el: "#report",
    data: {
      user: {
        name: "",
      },
      isFramed: isFramed,
      report: [],
      dateFrom: "",
      dateTo: "",
      interval: "0",
      fieldWidth: isFramed
        ? Math.floor(0.95 * document.documentElement.clientWidth)
        : Math.floor((document.documentElement.clientWidth * 3) / 4),
      fieldHeight: isFramed
        ? Math.floor(0.95 * document.documentElement.clientHeight)
        : Math.floor((document.documentElement.clientHeight * 2) / 3),
      intervalOptions: [
        { name: "0", title: "День" },
        { name: "1", title: "Неделя" },
        { name: "2", title: "Месяц" },
      ],
      axsisLines: [],
    },
    computed: {
      getLine: function () {
        let result = "";
        if (!this.report.length) return result;

        let columnWidth = this.fieldWidth / this.report.length;

        for (let i = 0; i < this.report.length; i++) {
          if (!result == "") result += " ";
          result +=
            String((i + 0.5) * columnWidth) +
            "," +
            String(
              Math.max(
                2,
                (1 - this.report[i].completed) * 0.8 * this.fieldHeight
              )
            );
        }
        return result;
      },
      getPoints: function () {
        let result = [];
        let columnWidth = this.fieldWidth / this.report.length;

        for (let i = 0; i < this.report.length; i++) {
          result.push({
            cx: (i + 0.5) * columnWidth,
            cy: (1 - this.report[i].completed) * 0.8 * this.fieldHeight,
            value: Math.floor(100 * this.report[i].completed),
          });
        }

        return result;
      },
      dates: function () {
        let result = [];
        if (!this.report.length) return result;

        let columnWidth = this.fieldWidth / this.report.length;

        for (let i = 0; i < this.report.length; i++) {
          let date = {
            y: this.fieldHeight * 0.9,
            x: (i + 0.5) * columnWidth,
            text: this.formatDate(new Date(this.report[i].date), "date"),
            size: Math.min(14, (0.2 * this.fieldWidth) / this.report.length),
          };
          result.push(date);
        }
        return result;
      },
    },
    methods: {
      initApp: async function () {
        let now = new Date();
        let weekAgo = new Date();
        weekAgo.setDate(weekAgo.getDate() - 7);
        this.dateTo = this.formatDate(now, "inputDate");
        this.dateFrom = this.formatDate(weekAgo, "inputDate");
        this.loadReport();
        this.user = await sg_multitask_common.getUser();
        let fieldAxsis = 4;
        for (let i = 0; i <= fieldAxsis; i++) {
          let level = (i * 0.8 * this.fieldHeight) / fieldAxsis;
          this.axsisLines.push({
            x1: 0,
            x2: this.fieldWidth,
            y1: level,
            y2: level,
            value: Math.floor((100 * (fieldAxsis - i)) / fieldAxsis),
          });
        }
      },
      loadReport: async function () {
        this.report = await this.apiReques("json_reportFinished.json", {
          method: "POST",
          body: JSON.stringify({
            interval: +this.interval,
            dateFrom: this.dateFromStr(this.dateFrom),
            dateTo: this.dateFromStr(this.dateTo),
          }),
          headers: { "Content-Type": "application/json" },
        });
      },
      formatDate: sg_multitask_common.formatDate,
      dateFromStr: sg_multitask_common.dateFromStr,
      apiReques: sg_multitask_common.apiReques,
    },
  });

  app.initApp();
});
