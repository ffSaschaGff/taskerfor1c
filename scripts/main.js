let sg_multitask_common = {
  onecPath: "../Web1C/hs/WebAccess/BasicPages/",

  getUser: async function () {
    return await sg_multitask_common.apiReques("json_user.json");
  },
  formatDate: function (date, format) {
    var dd = date.getDate();
    if (dd < 10) dd = "0" + dd;

    var mm = date.getMonth() + 1;
    if (mm < 10) mm = "0" + mm;

    var yy = date.getFullYear() % 100;
    if (yy < 10) yy = "0" + yy;

    var min = date.getMinutes();
    if (min < 10) min = "0" + min;

    var hour = date.getHours();
    if (hour < 10) hour = "0" + hour;

    if (format == "date") {
      return dd + "." + mm + "." + yy;
    } else if (format == "time") {
      return hour + ":" + min;
    } else if (format == "timeDate") {
      return hour + ":" + min + " " + dd + "." + mm + "." + yy;
    } else if (format == "inputDate") {
      return date.getFullYear() + "-" + mm + "-" + dd;
    } else {
      return dd + "." + mm + "." + yy + " " + hour + ":" + min;
    }
  },
  dateFromStr(str) {
    let dateElements = str.split("-");
    dateElements[1] = dateElements[1] - 1;
    return new Date(...dateElements);
  },
  async apiReques(url, requestParams) {
    let response = await fetch(
      sg_multitask_common.onecPath + url,
      requestParams
    );
    if (!response.ok) {
      sg_multitask_common.onApiRequestError();
      return;
    }
    try {
      let json = await response.json();
      return json;
    } catch {
      sg_multitask_common.onApiRequestError();
    }
  },
  onApiRequestError() {
    alert("Ошибка при выполнении запроса к API! Обратитесь в поддержке");
  },
  frameLoaded(event, timeout, callback) {
    let target = event.target;
    let element = event.target.contentDocument.documentElement;
    dateStart = Date.now();
    let timeoutHandler = function () {
      target.style.height = element.scrollHeight + "px";
      if (callback) callback();
      if (Date.now() < dateStart + timeout) {
        setTimeout(timeoutHandler, 250);
      }
    };
    setTimeout(timeoutHandler, 250);
  },
};
