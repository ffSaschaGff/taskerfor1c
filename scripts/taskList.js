let app;
document.addEventListener("DOMContentLoaded", function () {
  app = new Vue({
    el: "#tasksList",
    data: {
      little: false,
      tasks: [],
      unfinishadTasks: [],
      day: new Date(),
      newRow: {
        description: "",
        delay: 0,
      },
      user: {
        name: "",
      },
      currentCongratulation: -1,
      congratulations: [
        "Даже путь в тысячу ли начинается с одного шага",
        "Вода камень точит!",
        "Из тех ударов, что ты не сделал, 100% — мимо ворот",
        "Еще одна задача выполненна!",
        "Просто так даже кошки не родятся",
        "Делать надо как надо, а как не надо - не надо",
        "Еще один шаг к успеху!",
      ],
      rowsBgAnimationIsOn: true,
      statusOptions: [
        { name: "Завершена", title: "Завершена" },
        { name: "Отложена", title: "Отложена" },
        { name: "ВРаботе", title: "В работе" },
        { name: "МнеСообщили", title: "Мне сообщили" },
      ],
    },
    methods: {
      initApp: async function () {
        this.loadTasks();
        this.user = await sg_multitask_common.getUser();
        this.little = window.innerWidth < 600;
        if (this.little) {
          this.shrinkForMobile();
        }
      },
      shrinkForMobile: function () {
        this.statusOptions = [
          { name: "Завершена", title: "✅" },
          { name: "Отложена", title: "⏸" },
          { name: "ВРаботе", title: "💻" },
          { name: "МнеСообщили", title: "🔔" },
        ];
      },
      loadTasks: async function () {
        this.rowsBgAnimationIsOn = false;
        this.tasks = await this.apiReques("json_tasks.json", {
          method: "POST",
          body: JSON.stringify({ date: this.day }),
          headers: { "Content-Type": "application/json" },
        });

        this.unfinishadTasks = await this.apiReques(
          "json_unfinishedTasks.json",
          {
            method: "POST",
            body: JSON.stringify({ date: this.day }),
            headers: { "Content-Type": "application/json" },
          }
        );
        this.rowsBgAnimationIsOn = true;

        this.calcTimer();
      },
      setDate: function (step) {
        if (step) {
          this.day = new Date(this.day.setDate(this.day.getDate() + step));
        } else {
          this.day = new Date();
        }
        this.loadTasks();
      },
      onStatusChange: function (row) {
        if (row.status == "Завершена" && this.currentCongratulation == -1) {
          this.currentCongratulation = Math.floor(
            Math.random() * this.congratulations.length
          );
          let self = this;
          setTimeout(() => {
            self.currentCongratulation = -1;
          }, 5000);
        }
      },
      changeDelay: function (row) {
        let timerMSec = Date.now() - new Date(row.data).getTime();
        let hour = timerMSec / 3_600_000;
        row.delay = +row.timer + hour;
      },
      calcTimer: function (shedule) {
        for (let row of this.tasks) {
          this.calcTimerForRow(row, shedule);
        }
      },
      calcTimerForRow(row, sedule) {
        let oldTimer = +row.timer;
        let timerMSec =
          this.delayedDate(new Date(row.data), +row.delay).getTime() -
          Date.now();
        let hour = timerMSec / 3_600_000;
        row.timer = timerMSec < 0 ? "" : Math.round(100 * hour) / 100;
        if (sedule && oldTimer != 0 && row.timer == "") {
          let audioElement = document.getElementById("sound_Attention");
          if (audioElement.paused) {
            audioElement.play();
          }
        }
      },
      delayedDate: function (date, delay) {
        let copiedDate = new Date(date.getTime());
        copiedDate.setHours(copiedDate.getHours() + Math.floor(delay));
        copiedDate.setMinutes(
          copiedDate.getMinutes() + 60 * (delay - Math.floor(delay))
        );
        return copiedDate;
      },
      getDelayStep: function (delay) {
        if (+delay <= 0.5) {
          return "0.25";
        } else if (+delay <= 1) {
          return "0.5";
        }
        return "1";
      },
      addRow: async function () {
        let newTask = {
          data: new Date().toISOString(),
          description: this.newRow.description,
          UID: this.newUID(),
          status: "МнеСообщили",
          delay: "",
          timer: "",
        };
        this.newRow = {
          description: "",
          delay: "",
        };
        await this.apiReques("json_setTask.json", {
          method: "POST",
          body: JSON.stringify(newTask),
          headers: { "Content-Type": "application/json" },
        });
        this.tasks.push(newTask);
      },
      deleteRow: async function (rowIndex) {
        let data = this.tasks[rowIndex].data;
        let UID = this.tasks[rowIndex].UID;
        await this.apiReques("json_deleteTask.json", {
          method: "POST",
          body: JSON.stringify({ data, UID }),
          headers: { "Content-Type": "application/json" },
        });
        this.tasks.splice(rowIndex, 1);
      },
      markTaskMoved: async function (data, UID) {
        await this.apiReques("json_markTaskMoved.json", {
          method: "POST",
          body: JSON.stringify({ data, UID }),
          headers: { "Content-Type": "application/json" },
        });
      },
      addTaskToToday: async function (index) {
        let [row] = this.unfinishadTasks.splice(index, 1);
        this.markTaskMoved(row.data, row.UID);
        row.data = new Date().toISOString();
        row.delay = 0;
        await this.apiReques("json_setTask.json", {
          method: "POST",
          body: JSON.stringify(row),
          headers: { "Content-Type": "application/json" },
        });
        this.tasks.push(row);
      },
      setRow: async function (rowIndex, table) {
        if (!table) table = this.tasks;
        await this.apiReques("json_setTask.json", {
          method: "POST",
          body: JSON.stringify(table[rowIndex]),
          headers: { "Content-Type": "application/json" },
        });
      },
      newUID: function () {
        // Public Domain/MIT
        var d = new Date().getTime(); //Timestamp
        var d2 =
          (performance && performance.now && performance.now() * 1000) || 0; //Time in microseconds since page-load or 0 if unsupported
        return "xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx".replace(
          /[xy]/g,
          function (c) {
            var r = Math.random() * 16; //random number between 0 and 16
            if (d > 0) {
              //Use timestamp until depleted
              r = (d + r) % 16 | 0;
              d = Math.floor(d / 16);
            } else {
              //Use microseconds since page-load if supported
              r = (d2 + r) % 16 | 0;
              d2 = Math.floor(d2 / 16);
            }
            return (c === "x" ? r : (r & 0x3) | 0x8).toString(16);
          }
        );
      },
      formatDate: sg_multitask_common.formatDate,
      apiReques: sg_multitask_common.apiReques,
      frameLoaded: sg_multitask_common.frameLoaded,
    },
  });
  app.initApp();
  setInterval(() => {
    app.calcTimer(true);
  }, 50_000);
});
